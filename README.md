<h1> Stickhandling hockey game! </h1>

This is python code for a game that is like a superdeker stickhandling game

For more information check out my playlist on youtube for an overview of the build and some gameplay videos:

https://www.youtube.com/playlist?list=PLe12xaflq0wmFDYUBj7HNKVyu6Guzozy0

The game runs on a rapsberry pi and uses an arduino to control 5V hall sensors
to register points.  

run these commands: 

```
cd /
git clone https://gitlab.com/modernhackerspace/hockeyGame.git
sudo nano /etc/rc.local
#in that file add this line at the bottom: 
python /hockeyGame/hockey1.py &
```

on the next reboot, the game will automatically load.  If connected to the pi, the game 
can be run from the commandline. 

The 'hockey1.py' and 'highscores' files must both be in the /hockeyGame/ directory.

