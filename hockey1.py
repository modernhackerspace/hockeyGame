#!/usr/bin/python
import LCD1602
import threading
LCD1602.init(0x27,1)
LCD1602.clear()
LCD1602.write(0,0,"Loading")
from time import time, sleep
from gpiozero import LED,Button
from random import randint
#import pyfirmata library and set up interaction with arduino
import pyfirmata
board = pyfirmata.Arduino('/dev/ttyUSB0')
#this starts reading from the pins so that the arduino checks the pins
it=pyfirmata.util.Iterator(board)
it.start()

#define the arduino sensors
sensor1 = board.get_pin('d:5:i')
sensor2 = board.get_pin('d:6:i')
sensor3 = board.get_pin('d:7:i')
sensor4 = board.get_pin('d:8:i')
sensor5 = board.get_pin('d:9:i')
sensor6 = board.get_pin('d:10:i')
sensor7 = board.get_pin('d:11:i')
sensor8 = board.get_pin('d:12:i')

#buzzer setup which is conneceted to the arduino needs to be a digital write
buzzer = board.get_pin('d:4:o')

##quit game
quit=0
#set up LEDs
led1 = LED(22)
led2 = LED(10)
led3 = LED(9)
led4 = LED(11)
led5 = LED(5)
led6 = LED(6)
led7 = LED(13)
led8 = LED(19)

#buttons
select = Button(17)
startButton  = Button(27)
back=Button(15)
 
#game variables
gameChoice=0
points=0
playGame=True
sensor=0
over=0
lapse=0
gameTime=0
maxPoints=0
#game logic

#setup
highScoreFile = open("/hockeyGame/highscores", "r")
highScores=highScoreFile.readlines()
highScoreFile.close()

#function to reset the highscores table
def resetScores():
    areyousure=0
    LCD1602.clear()
        LCD1602.write(0,0,"Are you sure?")
    LCD1602.write(0,10,"No")
    #loops to double check before resetting them 
    while(1==1):
        if(select.is_pressed or back.is_pressed):
                if(areyousure==0):
                LCD1602.write(0,10,"Yes")
                areyousure=1
            elif(areyousure==1):
                    LCD1602.write(0,10,"No ")
                areyousure=0
            sleep(0.3)
        if(startButton.is_pressed):
            sleep(0.3)
            break
    #if it is pressed then open the file, write default high or low values in the table
    if(areyousure==1):
        highScoreFile = open("/hockeyGame/highscores", "r")
        highScores=highScoreFile.readlines()
        highScoreFile.close()
        highScoreFile = open("/hockeyGame/highscores", "w")
        for i in range(1,5):
            highScores[i]='1\n'
        for i in range(6,10):
            highScores[i]='101\n'
        for i in range(11,15):
                highScores[i]='1\n'
        for i in range(16,21):
                highScores[i]='101\n'
        for item in highScores:
            highScoreFile.write(item)
        highScoreFile.close()
        LCD1602.clear()
            LCD1602.write(0,0,"Scores Erased")
        sleep(2)
    LCD1602.clear()

#simple main function... start with a light show, turn off lights then look the game forevs
def main():
    print("start main")
    
    ledShow()
    sleep(2)
    for i in range(1,9):
        ledOff(i)
    while playGame == True:
        gameType = menu()
        startGame(gameType)

#this is a countdown end of game warning timer that creates a seperate thread to beep 
#at 1 second intervals when less than 10 secs and .5 interval when less than 5 secs
#starts a new thread then calls itself which essentially restarts the timer
def timeUp():
  global lapse
  global gameTime
  #checks to see if it is between 5-10 secs left in the game
  if((gameTime-lapse)<10 and (gameTime-lapse)>5):
    t=threading.Timer(1.0, timeUp)
    t.start()
    buzz()
  #checks to see if it is less than 5 secs in the game
  if((gameTime-lapse)<5 and (gameTime-lapse) >0):
    u=threading.Timer(0.5,timeUp)
    u.start()
    buzz()

#end of game timer similar to that above but
#works when there is less than 5 points left
def timer2():
    global maxPoints
    global points
    if(points>=maxPoints):
        print("over")
        else:
        t=threading.Timer(0.5,timer2)
        t.start()
        buzz()
#this is the menu function which iterates through the 
#game options
def menu():
    global lapse
    global gameTime

    choosing = 1
    gameSettings=[0,0,0,0,0]
    gameChoice=0
    LCD1602.clear()
    LCD1602.write(0,0,"Choose option::")
    #main menu loop lets you push buttons until the choice is made.. 5 options
    while(choosing==1):
        if (select.is_pressed):
            gameChoice+=1
            if(gameChoice>=5):
                                gameChoice=0
            if(gameChoice ==1):
                LCD1602.write(0,1,"Regular     ")
            elif(gameChoice ==2):
                LCD1602.write(0,1,"Speed Timer ")
            elif(gameChoice ==4):
                LCD1602.write(0,1,"Ultimate    ")
            elif(gameChoice ==3):
                LCD1602.write(0,1,"Skillz      ")
            elif(gameChoice ==0):
                LCD1602.write(0,1,"Reset Scores")
            sleep(0.3)
                if (back.is_pressed):
                        gameChoice-=1
                        if(gameChoice<0):
                                gameChoice=4
                        if(gameChoice ==1):
                                LCD1602.write(0,1,"Regular     ")
                        elif(gameChoice ==2):
                                LCD1602.write(0,1,"Speed Timer ")
                        elif(gameChoice ==4):
                                LCD1602.write(0,1,"Ultimate    ")
                        elif(gameChoice ==3):
                                LCD1602.write(0,1,"Skillz      ")
                        elif(gameChoice ==0):
                                LCD1602.write(0,1,"Reset Scores")
                        sleep(0.3)

        #if the start button is pressed it either locks the value or resets scores
        if (startButton.is_pressed):
            if(gameChoice==0):
                sleep(0.3)
                resetScores()
                LCD1602.write(0,0,"Choose game:")
                LCD1602.write(0,1,"Reset Scores")

            else:
                choosing==0
                gameSettings[0]=gameChoice
                print(gameSettings[0])
                sleep(0.5)
                break
    #once the value of the game is chosen, this if statement further configures items for 
    # the game based on the game choice.

    #mode 1 regular is a score game to try and get as many points in the time chosen
    #as such you can choose how long the game is:30-60 secs
    if(gameSettings[0]==1):
        gameTime= 50
        LCD1602.clear()
        LCD1602.write(0,0,"Select Time")
        LCD1602.write(0,1,str(gameTime))
        while(choosing==1):
            if(select.is_pressed):
                gameTime+=10
                sleep(0.3)
            if(back.is_pressed):
                gameTime-=10
                sleep(0.3)

            
            if(gameTime<20):
                gameTime=60
            if(gameTime>60):
                gameTime=30
            LCD1602.write(0,1,str(gameTime))
            if(startButton.is_pressed):
                gameSettings[1]=gameTime
                print("The game time is: " + str(gameSettings[1]))
                choosing=0
                sleep(1)
    #game 2 is a speed time game - you try to get X many points as 
    #fast as you can and the game logs your time
    #as such you can choose the points (30-60)
    elif(gameSettings[0]==2):
        maxPoints = 50
        LCD1602.clear()
                LCD1602.write(0,0,"Select Points")
                LCD1602.write(0,1,str(maxPoints))
        while(choosing==1):
            if(select.is_pressed):
                maxPoints+=10
                sleep(0.3)
            if(back.is_pressed):
                maxPoints-=10
                sleep(0.3)
            if(maxPoints<30):
                maxPoints=60
            if(maxPoints>60):
                maxPoints=30
            LCD1602.write(0,1,str(maxPoints))
            if(startButton.is_pressed):
                gameSettings[1]=maxPoints
                print("Game 2 points: " +str(gameSettings[1]))
                choosing=0
                sleep(1)
    #ultimate game works by having a 'timeout' value that is set between 1.25-3secs to start
    #you must get 10 sensors in a row and not have the timeout value turn off the sensor before you.
    #if the sensor times out you get a strike and the point streak resets.  If you reach 10 in a row
    # your time decreases a bit (.2 secs if the timeout is between 2-3secs, 0.1 secs if it is between
    #1-2 secs and by 0.05) until you can't go anymore and your strikes reset to 0.  Once you hit 3 strikes
    #the game ends
    elif(gameSettings[0]==4):
        setTime=3.00
        LCD1602.clear()
                LCD1602.write(0,0,"Timeout Time")
                LCD1602.write(0,1,str(setTime))
        #choose the timeout value between 3.00 and 1.25secs
        while(choosing==1):
            if(select.is_pressed):
                setTime+=0.25
                LCD1602.clear()
                        LCD1602.write(0,0,"Timeout Time")
                sleep(0.3)
            if(back.is_pressed):
                setTime-=0.25
                LCD1602.clear()
                        LCD1602.write(0,0,"Timeout Time")
                sleep(0.3)
            if(setTime>3.00):
                setTime=1.00
            if(setTime<1.00):
                setTime=3.00
            LCD1602.write(0,1,str(setTime))
            if(startButton.is_pressed):
                gameSettings[2]=setTime
                print("the timeout time is: "+str(gameSettings[2]))
                sleep(1)
                choosing=0
    #game three is a skill trainer.  Only 2 lights are lit and you practice the movement
    #like game 2..  X score as fast as you can.  During this config mode, you set which
    #two lights you have lit 
    elif(gameSettings[0]==3):
        maxPoints = 50
                LCD1602.clear()
                LCD1602.write(0,0,"Skillz")
                LCD1602.write(0,1,"points: "+str(maxPoints))
        #sets the max points for this mode between 50-100
                while(choosing==1):
                        if(select.is_pressed):
                                maxPoints+=10
                                sleep(0.3)
            if(back.is_pressed):
                maxPoints-=10
                sleep(0.3)
            if(maxPoints<50):
                maxPoints=100
                        if(maxPoints>100):
                                maxPoints=50
                        LCD1602.write(0,1,"points: "+str(maxPoints))
                        if(startButton.is_pressed):
                                gameSettings[1]=maxPoints
                                print("Game 2 points: " +str(gameSettings[1]))
                                choosing=0
                sleep(0.3)
                choosing=1
                selectLed=1
                selectLed2=0
        LCD1602.clear()
                LCD1602.write(0,0,"Choose Point")
        ledOn(selectLed)
        #choose the first point
        while(choosing==1):
                        if(select.is_pressed):
                                ledOff(selectLed)
                                selectLed+=1
                                if(selectLed>8):
                                        selectLed=1
                                ledOn(selectLed)
                                sleep(0.3)

                        if(startButton.is_pressed):                
                gameSettings[3]=selectLed
                sleep(0.3)
                break

            LCD1602.clear()
                LCD1602.write(0,0,"Choose Point 2")
        selectLed2=selectLed+1
        ledOn(selectLed2)
        #choose the second point
        while(choosing==1):
                        if(select.is_pressed):
                                ledOff(selectLed2)

                                selectLed2+=1
                if(selectLed2==selectLed):
                    selectLed2+=1
                                if(selectLed2>8):
                                        selectLed2=1
                    if(selectLed2==selectLed):
                        selectLed2+=1
                                ledOn(selectLed2)
                                sleep(0.3)
                        if(back.is_pressed):
                                ledOff(selectLed2)
                                selectLed2-=1
                                if(selectLed2<1):
                                        selectLed2=8
                                ledOn(selectLed2)
                                sleep(0.3)

                        if(startButton.is_pressed):
                gameSettings[4]=selectLed2
                                break
    #returns the game settings into the main game
    return gameSettings

def quitgame():
    global quit
    quit=1

# this is the main game loop and longest block of code
def startGame(gameSettings):
    #sets global variables that are passed amongst different functions
    global led1,led2,led3
    global sensor1,sensor2,sensor3
    global points
    global sensor
    global lapse
    global maxPoints
    global quit
    quit=0
    lastSensor =0
    point1 = gameSettings[3]
    point2 = gameSettings[4]
    ledOff(point1)
    ledOff(point2)
    gameType=gameSettings[0]
    gameTime=gameSettings[1]
    timeoutTime=gameSettings[2]
    maxPoints=gameSettings[1]
    waitForSensor=0
    lapse = 0.0
    LCD1602.clear()
        LCD1602.write(0,0,"Hit lit Sensor")
    LCD1602.write(0,1,"to start")
    led2.on()
    stageGame=1
    #the game "stages" and waits for you to hit a sensor before it starts
    while(stageGame==1):
        #print(sensor3.read())
        if(sensor2.read()==False):
            buzz()
            led2.off()
            lastSensor=2        #keeps track of the last sensor so that it doesn't re-light 
            startTime = time()  #starts the game timer
            break           
    LCD1602.clear()
    LCD1602.write(0,0,"GO GO GO GO GO")
    LCD1602.write(0,1,str(points))
    #main code for the "regular" game
    if(gameType==1):
        bool=1
        #keeps looping while the game time is less than the set time
        while(lapse<gameTime and quit==0):
            endTime=time()
            lapse = endTime-startTime
            print("Last Sensor was: " + str(lastSensor))
            sensor = randint(1,8)
            if(lastSensor==sensor):
                print("same sensor picking again")
                #little loop to run that randomly picks a sensor and ensures it isnt the same
                #as the previous sensor
                while(lastSensor==sensor):
                    sensor=randint(1,8)
            print("sensor is: " +str(sensor))
            #loops until the sensor hits and breaks
            while(waitForSensor ==0):
                if(back.is_pressed):
                    quit=1
                    break
                endTime=time()
                lapse=endTime-startTime
                if(bool==1 and (gameTime-lapse)<10):
                    timeUp()
                    bool=0
                #will break this loop to end the game if time expires in this loop                          
                if(lapse>=gameTime):
                    break
                #if the sensor is hit, it will register a point, beep, then turn off the light
                #breaks the loop into the random generator to pick a new sensor
                if(pickSensor(sensor)==False):
                    points+=1
                    print("sensor hit val is:")
                    print(sensor)
                    LCD1602.write(0,1,str(points))
                    buzz()
                    ledOff(sensor)
                    lastSensor=sensor
                    break
        #if time expires the game ends and calls the end of this game function
        endGame1()

    #this is the main code for the "speed time" game
    elif(gameType==2):
        bool=1
        #loops as long as the points is below the max set value
        while(points<maxPoints and quit==0):
            #if the points are less than 5 it will call the timer function. this uses
            # a true/fals value so it only runs once since the time is perpetual once it has been   
            #called.   
            if((maxPoints-points)<5 and bool==1):
                timer2()
                bool=0
            endTime=time()
            lapse=endTime-startTime
            lapse = '%.2f'%lapse
            LCD1602.write(0,1,str(lapse))
            print("Last Sensor was: " + str(lastSensor))
                    sensor = randint(1,8)
                    if(lastSensor==sensor):
                        print("same sensor picking again")
                #same as other games looping until the sensor is different
                            while(lastSensor==sensor):
                                sensor=randint(1,8)
                        print("sensor is: " +str(sensor))
            #will buzz and increase the points if it hit
            while(waitForSensor ==0):
                if(back.is_pressed):
                    quit=1
                    break

                if(pickSensor(sensor)==False):
                    points+=1
                    buzz()
                    ledOff(sensor)
                    lastSensor=sensor
                    break
        #calculates the end time once the point value is hit and uses this as the score
        #and passes it to the end of game function
        endTime = time()
        finalTime = endTime-startTime
        if(quit==1):
            finalTime=100
        endGame2(finalTime)
    #this is the main code for "ultimate"
    elif(gameType==4):
        alarmTest=0.0
        alarmVal=0.0
        bool=1
        strikes=0
        LCD1602.clear()
                LCD1602.write(0,0,"Strkes:"+str(strikes))
                LCD1602.write(0,1,"Point: "+str(points))
                                                                
        print("timeout Val:" + str(timeoutTime))
        #runs the main code as long as 3 strikes is not obtained
        while(strikes<3 and quit==0):
            #if 10 points is hit, then it resets the strikes and points and decreases the
            #time value based on what it is currently set as described in the settings  
            if(points==10):
                    points=0
                    strikes=0
                    #turns off all leds
                    if(timeoutTime<=3 and timeoutTime>2):
                        timeoutTime-=0.20
                    elif(timeoutTime<=2 and timeoutTime>1):     
                        timeoutTime-=0.10
                    elif(timeoutTime<=1 ):
                        timeoutTime-=0.05
                    LCD1602.clear()
                        LCD1602.write(0,0,"Next Level!")
                        LCD1602.write(0,1,"Time is: "+str(timeoutTime))
                    #sets display and does a little light show 
                    ledShow()
                        for i in range(0,9):
                        ledOff(i)
                        led2.on()
                        stageGame=1
                        #restages to allow the games to re-gain focus for the next round
                        while(stageGame==1):
                        if(sensor2.read()==False):
                                            buzz()
                                            led2.off()
                                            lastSensor=2
                                strikes=0
                                                                LCD1602.clear()
                                                    LCD1602.write(0,0,"Strkes: "+str(strikes))
                                                    LCD1602.write(0,1,"Points: "+str(points))
                                startTime = time()
                                break
            print("Last Sensor was: " + str(lastSensor))
                        sensor = randint(1,8)
                        if(lastSensor==sensor):
                                print("same sensor picking again")
                                while(lastSensor==sensor):
                                        sensor=randint(1,8)
                        print("sensor is: " +str(sensor))
            alarmTest=time()
            
            while(waitForSensor ==0):
                        if(back.is_pressed):
                    quit=1
                    break
                endTime=time()
                lapse=endTime-startTime
                if(strikes>=3):
                    break
                alarmVal=endTime-alarmTest
                if(alarmVal>=timeoutTime):
                    print("too slow")
                    lastSensor=sensor
                    ledOff(sensor)
                    strikes+=1
                    LCD1602.write(0,0,"Strkes: "+str(strikes))
                    points=0
                    LCD1602.write(0,1,"Points: "+str(points)+" ")
                    buzz()
                    buzz()
                    break
                if(pickSensor(sensor)==False):
                                        points+=1
                    strikes=0
                    LCD1602.write(0,0,"Strkes: "+str(strikes))
                                        LCD1602.write(0,1,"Points: "+str(points)+" ")
                                        buzz()
                                        ledOff(sensor)
                                        lastSensor=sensor
                                        break
        endGame3(timeoutTime)

    elif(gameType==3):
        bool=1
                while(points<maxPoints and quit==0):
                        if((maxPoints-points)<5 and bool==1):
                                timer2()
                                bool=0
                        endTime=time()
                        lapse=endTime-startTime
                        lapse = '%.2f'%lapse
            LCD1602.write(0,0,"points left: "+str(maxPoints-points))
                        LCD1602.write(0,1,"Time: "+str(lapse))
                        print("Last Sensor was: " + str(lastSensor))
                        if(sensor==point1):
                sensor=point2
            else:
                sensor=point1
                        print("sensor is: " +str(sensor))
                        while(waitForSensor ==0):
                        if(back.is_pressed):
                    quit=1
                    break

                                if(pickSensor(sensor)==False):
                                        points+=1
                                        strikes=0
                    buzz()
                                        ledOff(sensor)
                    LCD1602.clear()
                                        lastSensor=sensor
                                        break
                endTime = time()
                finalTime = endTime-startTime
        if(quit==1):
            finalTime=100
                endGame4(finalTime)

def endGame1():
    global points
    global highScores
    global gameTime
    highScoreIndex=0
    if(gameTime==30):
        print("hs index 1")
        highScoreIndex=1
    elif(gameTime==40):
        highScoreIndex=2
    elif(gameTime==50):
        highScoreIndex=3
    elif(gameTime==60):
        highScoreIndex=4
    else:
        print("Time Error")
            
    LCD1602.clear()
    LCD1602.write(0,0,"Game OVER")
    LCD1602.write(0,1,"Score is:")
    LCD1602.write(11,1,str(points))
    checkVal=highScores[highScoreIndex].strip()
    print(highScores[highScoreIndex])
    print("highScores")
    print(highScores)
    
    if(int(checkVal)<points):
        LCD1602.clear()
            LCD1602.write(0,0,"New High Score!")
            LCD1602.write(0,1,"Score is:")
            LCD1602.write(11,1,str(points))
        ledShow()
        highScores[highScoreIndex]=str(points)+'\n'
        writeFile(highScores)
    else:
        LCD1602.clear()
                LCD1602.write(0,0,"High Score:"+str(checkVal))
                LCD1602.write(0,1,"Your Score:"+str(points))
                LCD1602.write(11,1,str(points))
        sleep(2)
    resetGame()
    for i in range(1,9):
        ledOff(i)
    sleep(5)



def endGame3(timeoutTime):
        global points
        global highScores
        global gameTime
        highScoreIndex=11
    checkVal=highScores[highScoreIndex].strip()
        LCD1602.clear()
        LCD1602.write(0,0,"Game OVER")
        LCD1602.write(0,1,"Time is:")
        LCD1602.write(10,1,str(timeoutTime))
    sleep(3)
        if(int(checkVal)<points):
                LCD1602.clear()
                LCD1602.write(0,0,"New High Score!")
                LCD1602.write(0,1,"Timeout Time:")
                LCD1602.write(10,1,str(timeoutTime))
                ledShow()
                highScores[highScoreIndex]=str(points)+'\n'
                writeFile(highScores)
    else:
                LCD1602.clear()
                LCD1602.write(0,0,"High Score:"+str(checkVal))
                LCD1602.write(0,1,"Your Score:"+str(timeoutTime))
        sleep(2)
        resetGame()
        for i in range(1,9):
                ledOff(i)
    
def writeFile(highScores):
    highScoreFile=open("/hockeyGame/highscores","w")
    for score in highScores:
        highScoreFile.write(str(score))
    highScoreFile.close()
            
def endGame2(finalTime):
    global maxPoints
    global highScores
    highScoreIndex=0
    if(maxPoints==30):
                highScoreIndex=6
        elif(maxPoints==40):
                highScoreIndex=7
        elif(maxPoints==50):
                highScoreIndex=8
        elif(maxPoints==60):
                highScoreIndex=9
        else:
                print("Time Error")
        checkVal=highScores[highScoreIndex].strip()
    LCD1602.clear()
        LCD1602.write(0,0,"Game OVER")
        LCD1602.write(0,1,"Time is:")
        LCD1602.write(10,1,str(finalTime))
    if(checkVal>finalTime):
        LCD1602.clear()
                LCD1602.write(0,0,"New Record Time!")
                LCD1602.write(0,1,"time is:")
                LCD1602.write(10,1,str(finalTime))
        ledShow()
        highScores[highScoreIndex]=str(finalTime)+'\n'
                writeFile(highScores)
    else:
                LCD1602.clear()
                LCD1602.write(0,0,"Low Time:"+str(checkVal))
                LCD1602.write(0,1,"Your Time:"+str(finalTime))
                LCD1602.write(10,1,str(points))
        sleep(2)
    for i in range(1,9):
        ledOff(i)
    resetGame()
        sleep(5)

def endGame4(finalTime):
        global maxPoints
        global highScores
        highScoreIndex=0
        if(maxPoints==30):
                highScoreIndex=13
        elif(maxPoints==40):
                highScoreIndex=14
        elif(maxPoints==50):
                highScoreIndex=15
        elif(maxPoints==60):
                highScoreIndex=16
        else:
                print("Time Error")
    checkVal=highScores[highScoreIndex].strip()
        LCD1602.clear()
        LCD1602.write(0,0,"Game OVER")
        LCD1602.write(0,1,"Time is:")
        LCD1602.write(10,1,str(finalTime))
    sleep(3)
        if(checkVal>finalTime):
                LCD1602.clear()
                LCD1602.write(0,0,"New Record Time!")
                LCD1602.write(0,1,"Time is:")
                LCD1602.write(10,1,str(finalTime))
                ledShow()
                highScores[highScoreIndex]=str(finalTime)+'\n'
                writeFile(highScores)
    else:
                LCD1602.clear()
                LCD1602.write(0,0,"Low Time:"+str(checkVal))
                LCD1602.write(0,1,"Your Time:"+str(finalTime))
                LCD1602.write(10,1,str(points))
                sleep(2)
        for i in range(1,9):
                ledOff(i)
        resetGame()
        sleep(5)

def resetGame():
    global points
    global sensor
    global maxPoints
    global lapse
    lapse=0
    maxPoints=0
    sensor = 0
    points = 0
    for i in range(1,9):
        ledOff(i)


def pickSensor(sensorVal):
    sensorRead=0;
    if(sensorVal==1):
        sensorRead=sensor1.read()
        led1.on()
    elif(sensorVal==2):
        sensorRead=sensor2.read()
        led2.on()
    elif(sensorVal==3):
        sensorRead=sensor3.read()
        led3.on()
    elif(sensorVal==4):
        sensorRead=sensor4.read()
        led4.on()
    elif(sensorVal==5):
        sensorRead=sensor5.read()
        led5.on()
    elif(sensorVal==6):
        sensorRead=sensor6.read()
        led6.on()
    elif(sensorVal==7):
        sensorRead=sensor7.read()
        led7.on()
    elif(sensorVal==8):
        sensorRead=sensor8.read()
        led8.on()
    return sensorRead


def ledOff(sensorVal):
    if(sensorVal==1):
                led1.off()
        elif(sensorVal==2):
                led2.off()
        elif(sensorVal==3):
                led3.off()
        elif(sensorVal==4):
                led4.off()
        elif(sensorVal==5):
                led5.off()
        elif(sensorVal==6):
                led6.off()
        elif(sensorVal==7):
                led7.off()
        elif(sensorVal==8):
                led8.off()


def buzz():

    buzzer.write(1)
    sleep(0.1)
    buzzer.write(0)

def ledShow():
    show = randint(0,4)
    lapse=0.0

    def group1On():
        ledOn(1)
        ledOn(3)
        ledOn(5)
        ledOn(7)
    def group2On():
        ledOn(2)
        ledOn(4)
        ledOn(6)
        ledOn(8)
    def group2Off():
        ledOff(2)
        ledOff(4)
        ledOff(6)
        ledOff(8)
    def group1Off():
        ledOff(1)
        ledOff(3)
        ledOff(5)
        ledOff(7)
    def group3On():
        ledOn(2)
        ledOn(3)
        ledOn(4)
    def group3Off():
        ledOff(2)
        ledOff(3)
        ledOff(4)
    def group4On():
                ledOn(6)
                ledOn(7)
                ledOn(8)
        def group4Off():
                ledOff(6)
                ledOff(7)
                ledOff(8)
    startShow=time()
    if(show ==0):
        print("1")
        while(lapse<5):
            endShow=time()
            lapse=endShow-startShow
            light=randint(1,9)
            ledOn(light)
            sleep(0.1)
            ledOff(light)
    elif(show==1):
        print("2")
        while(lapse<5):
            endShow=time()
                        lapse=endShow-startShow
            group1On()
            group2Off()
            sleep(0.5)
            group2On()
            group1Off()
            sleep(0.5)
    elif(show==2):
        print("3")
        while(lapse<5):
            endShow=time()
                        lapse=endShow-startShow
            for i in range(1,9):
                ledOn(i)
                sleep(0.1)
                ledOff(i)

    elif(show==3):
        print("4")
        while(lapse<5):
            endShow=time()
            lapse=endShow-startShow
            ledOn(1)
            ledOn(4)
            ledOn(6)
            ledOff(3)
            ledOff(5)
            ledOff(7)
            sleep(0.5)
            ledOff(1)
            ledOff(4)
            ledOff(6)
            ledOn(3)
            ledOn(5)
            ledOn(7)
            sleep(0.5)
    elif(show==4):
        while(lapse<5):
            endShow=time()
                        lapse=endShow-startShow
            group4On()
            group3Off()
            sleep(0.3)
            group4Off()
    	    group3On()
            sleep(0.3)
def ledOn(num):
    if(num==1):
        led1.on()
    elif(num==2):
        led2.on()
    elif(num==3):
        led3.on()
    elif(num==4):
        led4.on()
    elif(num==5):
        led5.on()
    elif(num==6):
        led6.on()
    elif(num==7):
        led7.on()
    elif(num==8):
        led8.on()


main()

